# switchtower

Provides permanent IRIs and [HTTP 303 redirects](https://en.wikipedia.org/wiki/HTTPRange-14#Use_of_HTTP_Status_Code_303_See_Other) to documents currently describing resources denoted by them. It uses [CUID](https://github.com/ericelliott/cuid) path of unique IRI.

## references

* [Cool URIs for the Semantic Web](https://www.w3.org/TR/cooluris/)

## TODO

* [ ] enable reporting errors to tracker
* [ ] use *code* from POST as redirect status code
* [ ] implement changing redirects
* [ ] replace *secret* with authentication
* [ ] different redirect based on Content Negotiation - [303 URIs forwarding to Different Documents](https://www.w3.org/TR/cooluris/#r303uri)
