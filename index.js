'use strict'

const fs = require('fs')
const Hapi = require('hapi')
const level = require('level')
const cuid = require('cuid')
const config = require('./config.json')

let tls = null

if (config.tls.key && config.tls.cert) {
  tls = {
    key: fs.readFileSync(config.tls.key),
    cert: fs.readFileSync(config.tls.cert)
  }
}

// Create LevelDB
const db = level(config.level)

// Create a server with a host and port
const server = new Hapi.Server()

server.connection({
  host: config.host,
  port: config.port,
  routes: { cors: { credentials: true, exposedHeaders: ['Location'] } },
  tls: tls
})

if (config.sentry) {
  server.register({
    register: require('hapi-raven'),
    options: {
      dsn: config.sentry
    }
  }, (err) => {
    if (err) {
      console.log('Failed loading hapi-raven')
    }
  })
}

// Add route for creating new redirects
server.route({
  method: 'POST',
  path: '/',
  handler: function (request, reply) {
    if (request.payload.secret === config.secret) {
      let location = request.payload.location
      let perma = config.prefix + cuid()
      db.put(perma, location, function (err) {
        if (err) {
          return reply(err)
        }
        return reply().code(201).header('Location', perma)
      })
    } else {
      return reply().code(401)
    }
  }
})

// Add route for redirecting
server.route({
  method: 'GET',
  path: '/{cuid}',
  handler: function (request, reply) {
    let perma = config.prefix + request.params.cuid
    db.get(perma, function (err, location) {
      if (err) {
        if (err.name === 'NotFoundError') {
          return reply().code(404)
        } else {
          return reply(err)
        }
      }
      return reply().code(303).header('Location', location)
    })
  }
})

// Start the server
server.start((err) => {
  if (err) {
    server.plugins['hapi-raven'].client.captureError(err)
  }
  console.log('Server running at:', server.info.uri)
  console.log('Server using:', server.info.protocol)
})
